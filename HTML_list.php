<h1>Unorder list</h1>
    <ul>
        <li>Bangladesh</li>
        <li>Pakistan</li>
        <li>India</li>

    </ul>

<h1>Unorder list circle style</h1>
<ul style="list-style-type: circle">
    <li>Bangladesh</li>
    <li>Pakistan</li>
    <li>India</li>

</ul>

<h1>Unorder list square style</h1>
<ul style="list-style-type: square">
    <li>Bangladesh</li>
    <li>Pakistan</li>
    <li>India</li>

</ul>


<h1>Unorder list none style</h1>
<ul style="list-style-type: none">
    <li>Bangladesh</li>
    <li>Pakistan</li>
    <li>India</li>

</ul>


<h1>order list</h1>
<ol >
    <li>Bangladesh</li>
    <li>Pakistan</li>
    <li>India</li>

</ol>

<h1>order list type="A"</h1>
<ol type="A">
    <li>Bangladesh</li>
    <li>Pakistan</li>
    <li>India</li>

</ol>

<h1>order list type="a"</h1>
<ol type="a">
    <li>Bangladesh</li>
    <li>Pakistan</li>
    <li>India</li>

</ol>

<h1>order list type="i"</h1>
<ol type="i">
    <li>Bangladesh</li>
    <li>Pakistan</li>
    <li>India</li>

</ol>