<h1>Heading tags</h1>
<h1>This is heading tag h1</h1>
<h2>This is heading tag h2</h2>
<h3>This is heading tag h3</h3>
<h4>This is heading tag h4</h4>
<h5>This is heading tag h5</h5>
<h6>This is heading tag h6</h6>


<h1>Small tag</h1>
<p>This text is large but <small>This text is small</small></p>

<h1>Paragraph tag</h1>
<p>This is  paragraph.</p>

<h1>Marked text tag</h1>
<mark>highlight</mark>

<h1>Deleted text tag</h1>
<del>This line of text is meant to be treated as deleted text.</del>


<h1>Underlined,Bold,Italic tags</h1>
<u><b><i>this text is underlined.</i></b></u>

<h1>Blockquote tag</h1>
<blockquote>This is a long quotation.
    This is a long quotation.This is a long quotation.
    This is a long quotation.</blockquote>

<h1>abbr tag</h1>
The <abbr title="World Health Organization">WHO</abbr> was founded in 1948.

<h1>acronym tag</h1>
Do the work<acronym title="As Soon As Possible">
    ASAP</acronym>.

<h1>Address tag</h1>
<address>
    Written by pondit.com<br>
    <a href="mailto:team@pondit.com">Email us</a><br>
    Address: House#16,Road#12,Nikunjo<br>
    Phone: 01675******
</address>


<h1>bdo</h1>
<bdo dir="rtl">
This is LIVEOUTSOURCE.COM
</bdo>


<h1>big</h1>
<big>This text is big</big> but this is not.

<h1>cite</h1>
<cite>This Is a Citation</cite>

<h1>code</h1>
<p>Regular text. <code>This is code.</code> Regular text.</p>

<h1>dfn</h1>
<dfn>Definition term</dfn>

<p><dfn id="def-internet">The Internet</dfn> is a global system of interconnected networks that use the Internet Protocol Suite (TCP/IP) to serve billions of users worldwide.</p>

<h1>Inserted text</h1>

<p>I am <del>very</del><ins>extremely</ins> happy that you visited this page.</p>

<h1>pre tag</h1>
<pre>
    It preserves                both spaces
    and line breaks
</pre>

<h1>sub tag</h1>
<p>Chemical structure of water is H<sub>2</sub>O.</p>


<h1>sup tag</h1>
<p>Chemical structure of water is H<sup>2</sup>O.</p>



